import '../Constants.dart';

class UploadImageResponse {
  int responseCode;
  String responseData;
  UploadImageResponse({ this.responseCode, this.responseData });
  factory UploadImageResponse.fromJson(Map<String, dynamic> json) => _$UploadImageResponseFromJson(json);
  Map<String, dynamic> toJson() => _$UploadImageResponseToJson(this);
}

UploadImageResponse _$UploadImageResponseFromJson(Map<String, dynamic> json) {
  return UploadImageResponse(
    responseCode: json[ResponseKey.RESPONSE_CODE] as int,
    responseData: json[ResponseKey.RESPONSE_DATA][ResponseKey.MESSAGE] as String,
  );
}

Map<String, dynamic> _$UploadImageResponseToJson(UploadImageResponse instance) => <String, dynamic> {
  ResponseKey.RESPONSE_CODE: instance.responseCode,
  ResponseKey.RESPONSE_DATA: instance.responseData
};