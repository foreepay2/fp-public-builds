import '../Constants.dart';

class SignUpResponse {
  int responseCode;
  String errorMessage;
  String businessId;
  SignUpResponse({ this.responseCode, this.businessId, this.errorMessage });
  factory SignUpResponse.fromJson(Map<String, dynamic> json) => _$SignUpResponseFromJson(json);
  Map<String, dynamic> toJson() => _$SignUpResponseToJson(this);
}
SignUpResponse _$SignUpResponseFromJson(Map<String, dynamic> json) {
  print(json);
  return SignUpResponse(
    responseCode: json[JsonFieldKey.RESPONSE_CODE] as int,
    errorMessage: json[ResponseKey.RESPONSE_DATA][ResponseKey.ERROR_MESSAGE] as String,
    businessId: json[ResponseKey.RESPONSE_DATA][JsonFieldKey.BUSINESS_ID] as String,
  );
}

Map<String, dynamic> _$SignUpResponseToJson(SignUpResponse instance) => <String, dynamic> {
  ResponseKey.RESPONSE_CODE: instance.responseCode,
  JsonFieldKey.BUSINESS_ID: instance.businessId
};