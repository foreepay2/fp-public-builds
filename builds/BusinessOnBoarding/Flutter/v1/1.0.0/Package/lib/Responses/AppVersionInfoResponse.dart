import '../Constants.dart';

class AppVersionInfoResponse {
  String availableSDKVersion;
  bool encryptionRequired;
  bool isUpdateAvailable;
  String minimumSDKVersion;
  int responseCode;

  AppVersionInfoResponse( this.availableSDKVersion, this.encryptionRequired,
      this.isUpdateAvailable, this.minimumSDKVersion, this.responseCode );

  factory AppVersionInfoResponse.fromJson(dynamic json) {
    return AppVersionInfoResponse(
      json[JsonFieldKey.AVAILABLE_SDK_VERSION] as String,
      json[JsonFieldKey.ENCRYPTION_REQUIRED] as bool,
      json[JsonFieldKey.IS_UPDATE_AVAILABLE] as bool,
      json[JsonFieldKey.MINIMUM_APP_VERSION] as String,
      json[JsonFieldKey.RESPONSE_CODE] as int
    );
  }

  @override
  String toString() {
    return '{ ${this.availableSDKVersion}, ${this.encryptionRequired}, '
        '${this.isUpdateAvailable}, ${this.minimumSDKVersion}, '
        '${this.responseCode} }';
  }
}