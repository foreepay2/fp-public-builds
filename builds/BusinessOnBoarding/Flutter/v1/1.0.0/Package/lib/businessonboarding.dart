library businessonboarding;

import 'package:package_info/package_info.dart';

import 'Constants.dart';
import 'Responses/AppVersionInfoResponse.dart';
import 'Utils.dart';

class PrepareForm {
  Future<Map<String, String>> build(String apiKey) async {
    Map<String, String> data = new Map();
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    if (packageInfo != null) {
      AppVersionInfoResponse infoResponse = await Utilities.versionInfoCall(
          apiKey, packageInfo.version);
      if (infoResponse.responseCode == 200) {
        if (infoResponse.isUpdateAvailable)
          print("Please update foree's business onBoarding SDK, "
              "available version is: " + infoResponse.availableSDKVersion);
        final officePremisesJson = await Utilities.fetchOfficePremises(apiKey)
            .catchError((onError) => print(onError));
        final legalEntitiesJson = await Utilities.fetchLegalEntities(apiKey)
            .catchError((onError) => print(onError));
        final businessCategoriesJson = await Utilities.fetchBusinessCategories(
            apiKey).catchError((onError) => print(onError));
        data[General.MAP_KEY_OFFICE_PREMISES] = officePremisesJson;
        data[General.MAP_KEY_LEGAL_ENTITIES] = legalEntitiesJson;
        data[General.MAP_KEY_BUSINESS_CATEGORIES] = businessCategoriesJson;
      } else throw Exception(infoResponse.responseCode);
    } else throw Exception("Cannot read package info");
    return data;
  }
}
