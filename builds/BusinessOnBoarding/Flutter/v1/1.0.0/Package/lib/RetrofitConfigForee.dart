import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';

import 'Constants.dart';

part 'RetrofitConfigForee.g.dart';

@RestApi(baseUrl: "https://api-dev5.foreebill.com/")
abstract class ForeeRestClient {
  factory ForeeRestClient(Dio dio) = _ForeeRestClient;

  @POST("/sdk/on-boarding/version")
  Future<String> getVersion(@Body() VersionInfo versionInfo);

  @POST("/aggregator/business/signup")
  Future<String> signUp(@Body() UserDetails signUp);

  @GET("/static/static_business_legal_entity")
  Future<String> fetchLegalEntities();

  @GET("/static/static_business_category")
  Future<String> fetchBusinessCategories();

  @POST("/aggregator/business/upload-docs")
  Future<String> uploadImageToServer(@Body() FormData formData);

  @GET("/static/static_bank")
  Future<String> fetchBanks();

  @GET("/static/static_office_premises")
  Future<String> fetchOfficePremises();
}

@JsonSerializable()
class VersionInfo {
  String sdkPlatform;
  String sdkAppVersion;
  VersionInfo({ this.sdkPlatform, this.sdkAppVersion });
  factory VersionInfo.fromJson(Map<String, dynamic> json) =>
      _$VersionFromJson(json);
  Map<String, dynamic> toJson() => _$VersionToJson(this);
}

VersionInfo _$VersionFromJson(Map<String, dynamic> json) {
  return VersionInfo(
    sdkPlatform: json[JsonFieldKey.SDK_PLATFORM] as String,
    sdkAppVersion: json[JsonFieldKey.SDK_APP_VERSION] as String,
  );
}

Map<String, dynamic> _$VersionToJson(VersionInfo instance) =>
    <String, dynamic> {
  JsonFieldKey.SDK_PLATFORM: instance.sdkPlatform,
  JsonFieldKey.SDK_APP_VERSION: instance.sdkAppVersion,
};

@JsonSerializable()
class UserDetails {
  String authorizedSignatoryName;
  String authorizedSignatoryCnic;
  String businessId;
  String businessLegalName;
  String businessName;
  String businessCategory;
  String emailAddress;
  String expectedMonthlyTurnOver;
  String mobileNumber;
  String ntnCnicNumber;
  String officialWebsite;
  OfficePremises officePremises;
  String physicalAddress;
  String password;
  String registeredNumber;
  String registeredAddress;
  String yearAtCurrentLocation;
  String yearAtCurrentBusiness;
  BusinessDetails businessDetails;
  LegalEntities legalEntity;
  bool formSubmitted;

  UserDetails({this.authorizedSignatoryName, this.authorizedSignatoryCnic,
    this.businessId, this.businessLegalName, this.businessName,
    this.businessCategory, this.emailAddress, this.expectedMonthlyTurnOver,
    this.legalEntity, this.mobileNumber, this.ntnCnicNumber,
    this.officialWebsite, this.officePremises, this.physicalAddress,
    this.password, this.registeredNumber, this.registeredAddress,
    this.yearAtCurrentLocation, this.yearAtCurrentBusiness,
    this.businessDetails, this.formSubmitted});
  factory UserDetails.fromJson(Map<String, dynamic> json) =>
      _$UserDetailsFromJson(json);
  Map<String, dynamic> toJson() => _$UserDetailsToJson(this);
}

UserDetails _$UserDetailsFromJson(Map<String, dynamic> json) {
  return UserDetails(
    authorizedSignatoryName: json[JsonFieldKey
        .AUTHORIZED_SIGNATORY_NAME] as String,
    authorizedSignatoryCnic: json[JsonFieldKey
        .AUTHORIZED_SIGNATORY_CNIC] as String,
    businessLegalName: json[JsonFieldKey.BUSINESS_NAME] as String,
    businessCategory: json[JsonFieldKey.BUSINESS_CATEGORY] as String,
    emailAddress: json[JsonFieldKey.EMAIL_ADDRESS] as String,
    expectedMonthlyTurnOver: json[JsonFieldKey
        .EXPECTED_MONTHLY_TURNOVER] as String,
    legalEntity: LegalEntities.fromJson(
        json[JsonFieldKey.BUSINESS_LEGAL_ENTITY]),
    mobileNumber: json[JsonFieldKey.MOBILE_NUMBER] as String,
    ntnCnicNumber: json[JsonFieldKey.NTN] as String,
    officialWebsite: json[JsonFieldKey.OFFICIAL_WEBSITE] as String,
    officePremises: OfficePremises.fromJson(json[JsonFieldKey.OFFICE_PREMISES]),
    physicalAddress: json[JsonFieldKey.PHYSICAL_ADDRESS] as String,
    password: json[JsonFieldKey.PASSWORD] as String,
    registeredNumber: json[JsonFieldKey.REGISTERED_NUMBER] as String,
    registeredAddress: json[JsonFieldKey.REGISTERED_ADDRESS] as String,
    yearAtCurrentLocation: json[JsonFieldKey
        .YEARS_AT_CURRENT_LOCATION] as String,
    yearAtCurrentBusiness: json[JsonFieldKey.YEARS_IN_BUSINESS] as String,
    businessDetails: BusinessDetails.fromJson(json[JsonFieldKey.BUSINESS_DETAILS]),
    formSubmitted: json[JsonFieldKey.FORM_SUBMITTED] as bool
  );
}

Map<String, dynamic> _$UserDetailsToJson(UserDetails instance) => <String, dynamic> {
  JsonFieldKey.AUTHORIZED_SIGNATORY_NAME: instance.authorizedSignatoryName,
  JsonFieldKey.AUTHORIZED_SIGNATORY_CNIC: instance.authorizedSignatoryCnic,
  JsonFieldKey.BUSINESS_ID: instance.businessId,
  JsonFieldKey.BUSINESS_LEGAL_NAME: instance.businessLegalName,
  JsonFieldKey.BUSINESS_NAME: instance.businessLegalName,
  JsonFieldKey.BUSINESS_CATEGORY: instance.businessCategory,
  JsonFieldKey.EMAIL_ADDRESS: instance.emailAddress,
  JsonFieldKey.EXPECTED_MONTHLY_TURNOVER: instance.expectedMonthlyTurnOver,
  JsonFieldKey.BUSINESS_LEGAL_ENTITY: instance.legalEntity,
  JsonFieldKey.MOBILE_NUMBER: instance.mobileNumber,
  JsonFieldKey.NTN: instance.ntnCnicNumber,
  JsonFieldKey.OFFICIAL_WEBSITE: instance.officialWebsite,
  JsonFieldKey.OFFICE_PREMISES: instance.officePremises.toJson(),
  JsonFieldKey.PHYSICAL_ADDRESS: instance.physicalAddress,
  JsonFieldKey.PASSWORD: instance.password,
  JsonFieldKey.REGISTERED_NUMBER: instance.registeredNumber,
  JsonFieldKey.REGISTERED_ADDRESS: instance.registeredAddress,
  JsonFieldKey.YEARS_AT_CURRENT_LOCATION: instance.yearAtCurrentLocation,
  JsonFieldKey.YEARS_IN_BUSINESS: instance.yearAtCurrentBusiness,
  JsonFieldKey.BUSINESS_DETAILS: instance.businessDetails.toJson(),
  JsonFieldKey.FORM_SUBMITTED: instance.formSubmitted
};

@JsonSerializable()
class BusinessCategories {
  int id;
  String name;
  BusinessCategories({ this.id, this.name });
  factory BusinessCategories.fromJson(Map<String, dynamic> json) =>
      _$BusinessCategoriesFromJson(json);
  Map<String, dynamic> toJson() => _$BusinessCategoriesToJson(this);
}

BusinessCategories _$BusinessCategoriesFromJson(Map<String, dynamic> json) {
  return BusinessCategories(
      id: json[JsonFieldKey.ID] as int,
      name: json[JsonFieldKey.NAME] as String
  );
}

Map<String, dynamic> _$BusinessCategoriesToJson(BusinessCategories instance) =>
    <String, dynamic> {
  JsonFieldKey.ID: instance.id,
  JsonFieldKey.NAME: instance.name
};

@JsonSerializable()
class LegalEntities {
  int id;
  String name;
  LegalEntities({ this.id, this.name });
  factory LegalEntities.fromJson(Map<String, dynamic> json) =>
      _$LegalEntitiesFromJson(json);
  Map<String, dynamic> toJson() => _$LegalEntitiesToJson(this);
}

LegalEntities _$LegalEntitiesFromJson(Map<String, dynamic> json) {
  return LegalEntities(
      id: json[JsonFieldKey.ID] as int,
      name: json[JsonFieldKey.NAME] as String
  );
}

Map<String, dynamic> _$LegalEntitiesToJson(LegalEntities instance) =>
    <String, dynamic> {
  JsonFieldKey.ID: instance.id,
  JsonFieldKey.NAME: instance.name
};

@JsonSerializable()
class BusinessDetails {
  String accountTitle;
  String accountNumber;
  String bankName;
  String branchName;
  String branchCode;
  String iBan;
  BusinessDetails({ this.accountTitle, this.accountNumber, this.bankName,
    this.branchName, this.branchCode, this.iBan });
  factory BusinessDetails.fromJson(Map<String, dynamic> json) =>
      _$BusinessDetailsFromJson(json);
  Map<String, dynamic> toJson() => _$BusinessDetailsToJson(this);
}

BusinessDetails _$BusinessDetailsFromJson(Map<String, dynamic> json) {
  return BusinessDetails(
    accountTitle: json[JsonFieldKey.TITLE] as String,
    accountNumber: json[JsonFieldKey.ACCOUNT_NUMBER] as String,
    bankName: json[JsonFieldKey.BANK_NAME] as String,
    branchName: json[JsonFieldKey.BRANCH_NAME] as String,
    branchCode: json[JsonFieldKey.BRANCH_CODE] as String,
    iBan: json[JsonFieldKey.IBAN] as String
  );
}

Map<String, dynamic> _$BusinessDetailsToJson(BusinessDetails instance) =>
    <String, dynamic> {
  JsonFieldKey.TITLE: instance.accountTitle,
  JsonFieldKey.ACCOUNT_NUMBER: instance.accountNumber,
  JsonFieldKey.BANK_NAME: instance.bankName,
  JsonFieldKey.BRANCH_NAME: instance.branchName,
  JsonFieldKey.BRANCH_CODE: instance.branchCode,
  JsonFieldKey.IBAN: instance.iBan
};

@JsonSerializable()
class PartnersDetails {
  String partnerName, partnerCnic;
  PartnersDetails({ this.partnerName, this.partnerCnic });
  factory PartnersDetails.fromJson(Map<String, dynamic> json) => _$PartnersDetailsFromJson(json);
  Map<String, dynamic> toJson() => _$PartnersDetailsToJson(this);
}

PartnersDetails _$PartnersDetailsFromJson(Map<String, dynamic> json) {
  return PartnersDetails(
      partnerName: json[JsonFieldKey.PARTNER_NAME] as String,
      partnerCnic: json[JsonFieldKey.PARTNER_CNIC] as String
  );
}

Map<String, dynamic> _$PartnersDetailsToJson(PartnersDetails instance) =>
    <String, dynamic> {
  JsonFieldKey.PARTNER_NAME: instance.partnerName,
  JsonFieldKey.PARTNER_CNIC: instance.partnerCnic
};

@JsonSerializable()
class OfficePremises {
  int id;
  String name;
  OfficePremises({ this.id, this.name });
  factory OfficePremises.fromJson(Map<String, dynamic> json) =>
      _$OfficePremisesFromJson(json);
  Map<String, dynamic> toJson() => _$OfficePremisesToJson(this);
}

OfficePremises _$OfficePremisesFromJson(Map<String, dynamic> json) {
  return OfficePremises(
    id: json[JsonFieldKey.OFFICE_PREMISES_ID] as int,
    name: json[JsonFieldKey.OFFICE_PREMISES_NAME] as String
  );
}

Map<String, dynamic> _$OfficePremisesToJson(OfficePremises instance) =>
    <String, dynamic> {
  JsonFieldKey.OFFICE_PREMISES_ID: instance.id,
  JsonFieldKey.OFFICE_PREMISES_NAME: instance.name
};