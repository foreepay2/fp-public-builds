import 'dart:convert';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

import 'BusinessDocuments.dart';
import 'Responses/SignUpResponse.dart';
import 'Responses/UploadImageResponse.dart';
import 'Result.dart';
import 'Constants.dart' as Constant;
import 'RetrofitConfigForee.dart';
import 'Utils.dart';

class BusinessSignUpForm extends StatefulWidget {
  final String title;
  final Map<String, String> data;
  final String fieldsData;
  final String apiKey;
  BusinessSignUpForm({Key key, @required this.apiKey, this.title, this.data, this.fieldsData}) : super(key: key);

  @override
  _BusinessSignUpFormState createState() =>_BusinessSignUpFormState(apiKey, data, fieldsData);
}

class _BusinessSignUpFormState extends State<BusinessSignUpForm> {
  final _formKey = GlobalKey<FormState>();
  Widget logo;
  String businessLegalName, businessName;
  String registeredNumber, mobileNumber, emailAddress;
  String registeredAddress, physicalAddress, officialWebsite;
  String legalEntity, legalEntityOther;
  String ntnCnicNumber;
  String authorizedSignatoryName, authorizedSignatoryCnic;
  String businessCategory, businessCategoryOther, officeLocation;
  String yearsAtCurrentLocation, yearsAtCurrentBusiness;
  String expectedMonthlyTurnOver;
  String bankName, branchName, accountTitle, branchCode, accountNumber, iBan;
  String partnerName1 = "", partnerName2 = "", partnerName3 = "", partnerName4 = "";
  String partnerCnic1 = "", partnerCnic2 = "", partnerCnic3 = "", partnerCnic4 = "";
  String logoPath = "";
  Result rawResult;
  bool hasPartners = false;
  bool showLogoUploadIcon = true;
  bool showPartnersFields = false;
  bool isLegalEntityOtherFieldVisible = false;
  bool isBusinessCategoryOtherFieldVisible = false;
  bool isSaveButtonDisabled = true;
  bool isLogoUploaded = false;

  Map<int, String> legalEntitiesMap;
  Map<int, String> businessCategoriesMap;
  Map<int, String> officePremisesMap;

  UserDetails userDetails;
  BusinessDetails businessDetails = new BusinessDetails();
  OfficePremises officePremises = new OfficePremises();
  LegalEntities legalEntities = new LegalEntities();
  File file;

  _BusinessSignUpFormState(String apiKey, Map<String, String> data,
      String fieldsData) {
    if (data.containsKey(Constant.General.MAP_KEY_RESULT))
      rawResult = Result.fromJson(json.decode(
          data[Constant.General.MAP_KEY_RESULT]));
    if (data.containsKey(Constant.General.MAP_KEY_LEGAL_ENTITIES)) {
      legalEntitiesMap = new Map();
      for (var json in json.decode(
          data[Constant.General.MAP_KEY_LEGAL_ENTITIES])) {
        LegalEntities entities = LegalEntities.fromJson(json);
        legalEntitiesMap[entities.id] = entities.name;
      }
    }
    if (data.containsKey(Constant.General.MAP_KEY_BUSINESS_CATEGORIES)) {
      businessCategoriesMap = new Map();
      for (var json in json.decode(
          data[Constant.General.MAP_KEY_BUSINESS_CATEGORIES])) {
        BusinessCategories categories = BusinessCategories.fromJson(json);
        businessCategoriesMap[categories.id] = categories.name;
      }
    }
    if (data.containsKey(Constant.General.MAP_KEY_OFFICE_PREMISES)) {
      officePremisesMap = new Map();
      for (var json in json.decode(
          data[Constant.General.MAP_KEY_OFFICE_PREMISES])) {
        OfficePremises officePremises = OfficePremises.fromJson(json);
        officePremisesMap[officePremises.id] = officePremises.name;
      }
    }
    userDetails = new UserDetails();
    businessDetails = new BusinessDetails();
    if (fieldsData != null && fieldsData.isNotEmpty) {
      Map<String, dynamic> val = jsonDecode(fieldsData);
      if (val.containsKey(Constant.FieldKeys.BUSINESS_NAME))
        userDetails.businessName = val[Constant.FieldKeys.BUSINESS_NAME];
      if (val.containsKey(Constant.FieldKeys.CNIC))
        userDetails.ntnCnicNumber = val[Constant.FieldKeys.CNIC];
      if (val.containsKey(Constant.FieldKeys.MOBILE_NUMBER))
        userDetails.mobileNumber = val[Constant.FieldKeys.MOBILE_NUMBER];
      if (val.containsKey(Constant.FieldKeys.LOGO)) {
        String tempLogo = val[Constant.FieldKeys.LOGO];
        if (tempLogo.isNotEmpty)
          logo = Image.asset(tempLogo);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    if (widget.apiKey == null || widget.apiKey.isEmpty)
      throw Exception(Constant.General.API_KEY_REQUIRED);
    if (rawResult == null || legalEntitiesMap == null || businessCategoriesMap == null)
      throw Exception("Try again, Data are missing.");
    return Scaffold(appBar: AppBar(title: Text(widget.title), leading: logo,),
        body: Padding(
          padding: const EdgeInsets.only(left: Constant.Layout.left,
              right: Constant.Layout.right),
          child: Container(
            child: new Column(
              children: <Widget>[
                new Expanded(
                  child: new SingleChildScrollView(
                    child: new Form(
                      key: _formKey,
                      child: new Column(
                        children: [
                          // Business id
                          Padding(
                            padding: Constant.PaddingDetails.textTopPadding,
                            child: Text(userDetails.businessId == null ? "" : "Business_id: " + userDetails.businessId),
                          ),
                          // Business logo
                          Padding(
                              padding: Constant.PaddingDetails.textTopPadding,
                              child: new GestureDetector(
                                  onTap: () { uploadImage(FileType.image); },
                                  child: new Container(
                                    height: Constant.Layout.uploadLogoH,
                                    child: new Row(
                                      children: [
                                        new Container(
                                          width: Constant.Layout.uploadLogoW,
                                          height: MediaQuery.of(context).size.height,
                                          child: new Card(
                                            elevation: 0,
                                            color: Colors.transparent,
                                            child: Stack(
                                              children: [
                                                Center(
                                                  child: Image.file(
                                                    new File(logoPath),
                                                    fit: BoxFit.fill,
                                                  ),
                                                ),
                                                Visibility(
                                                  visible: logoPath.isNotEmpty,
                                                  child: Align(
                                                    alignment: Alignment.topRight,
                                                    child: GestureDetector(
                                                      onTap: () {
                                                        _removeLogo();
                                                      },
                                                      child: Icon(Icons.cancel_outlined),
                                                    ),
                                                  ),
                                                ),
                                                Center(
                                                  child: Opacity(
                                                      opacity: 0.5,
                                                      child: Visibility(
                                                          visible: !logoPath.isNotEmpty,
                                                          child: new Column(
                                                            mainAxisSize: MainAxisSize.min,
                                                            children: [
                                                              Icon(Icons.cloud_upload,
                                                                size: 50.0,
                                                              ),
                                                              Text(Constant.General.UPLOAD)
                                                            ],
                                                          )
                                                      )
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                            child: Column(
                                              children: [
                                                Padding(
                                                  padding: const EdgeInsets
                                                      .only(top: Constant.Layout.dp5,
                                                      left: Constant.Layout.textViewTop),
                                                  child: Align(
                                                    alignment: Alignment.topLeft,
                                                    child: Text(
                                                      Constant.RequiredDocs.logoText,
                                                      softWrap: true,
                                                      style: TextStyle(
                                                          fontWeight: FontWeight.bold,
                                                          fontSize: 17.0
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets.only(
                                                      top: Constant.Layout.dp5,
                                                      left: Constant.Layout.textViewTop
                                                  ),
                                                  child: Align(
                                                    alignment: Alignment.topLeft,
                                                    child: Text(
                                                      Constant.RequiredDocs.logoDescription,
                                                      softWrap: true,
                                                      style: TextStyle(
                                                          fontSize: 12.0
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            )
                                        ),
                                      ],
                                    ),
                                  )
                              )
                          ),
                          // Business legal name
                          Padding(
                              padding: Constant.PaddingDetails.textTopPadding,
                              child: TextFormField(
                                decoration: InputDecoration(
                                    border: const OutlineInputBorder(),
                                    labelText: Constant.FormLabels
                                        .BUSINESS_LEGAL_NAME
                                ),
                                validator: (val) {
                                  if (val.isEmpty)
                                    return Constant.ErrorFields.MANDATORY;
                                  else if (val.length < Constant.LengthLimit
                                      .MIN_LENGTH_5)
                                    return Constant.ErrorFields
                                        .MIN_LENGTH_ERROR_5;
                                  else if (val.length > Constant.LengthLimit
                                      .MAX_LENGTH_200)
                                    return Constant.ErrorFields
                                        .MAX_LENGTH_ERROR_200;
                                  else return null;
                                },
                                onChanged: (val) => userDetails
                                    .businessLegalName = val,
                                textCapitalization: TextCapitalization
                                    .sentences,
                              )
                          ),
                          // DBA (Doing Business As) Name
                          Padding(
                            padding: Constant.PaddingDetails.textTopPadding,
                            child: TextFormField(
                              decoration: InputDecoration(
                                  border: const OutlineInputBorder(),
                                  labelText: Constant.FormLabels.BUSINESS_NAME
                              ),
                              validator: (val) {
                                if (val.isEmpty) {
                                  return Constant.ErrorFields.MANDATORY;
                                } else if (val.length < Constant.LengthLimit
                                    .MIN_LENGTH_5)
                                  return Constant.ErrorFields
                                      .MIN_LENGTH_ERROR_5;
                                else if (val.length > Constant.LengthLimit
                                    .MAX_LENGTH_200)
                                  return Constant.ErrorFields
                                      .MAX_LENGTH_ERROR_200;
                                else return null;
                              },
                              initialValue: userDetails.businessName,
                              textCapitalization: TextCapitalization.sentences,
                              onChanged: (val) => userDetails.businessName = val,
                            ),
                          ),
                          // Registered Number
                          Padding(
                              padding: Constant.PaddingDetails.textTopPadding,
                              child: TextFormField(
                                  decoration: InputDecoration(
                                      border: const OutlineInputBorder(),
                                      labelText: Constant.FormLabels
                                          .REGISTERED_NUMBER,
                                      counterText: ""
                                  ),
                                  validator: (val) {
                                    if (val.isEmpty)
                                      return null;
                                    else if (val.length < Constant.LengthLimit
                                        .MIN_LENGTH_11)
                                      return Constant.ErrorFields
                                          .MIN_LENGTH_ERROR_11;
                                    else return null;
                                  },
                                  onChanged: (val) => userDetails.registeredNumber = val,
                                  keyboardType: TextInputType.number,
                                  maxLength: 11,
                                  maxLengthEnforced: true
                              )
                          ),
                          // Mobile Number
                          Padding(
                              padding: Constant.PaddingDetails.textTopPadding,
                              child: TextFormField(
                                  decoration: InputDecoration(
                                      border: const OutlineInputBorder(),
                                      labelText: Constant.FormLabels
                                          .MOBILE_NUMBER,
                                      counterText: ""
                                  ),
                                  validator: (val) {
                                    if (val.isEmpty)
                                      return Constant.ErrorFields.MANDATORY;
                                    else if (val.length < 11)
                                      return Constant.ErrorFields
                                          .MIN_LENGTH_ERROR_11;
                                    else return null;
                                  },
                                  keyboardType: TextInputType.number,
                                  onChanged: (val) => userDetails.mobileNumber = val,
                                  initialValue: userDetails.mobileNumber,
                                  maxLength: 11,
                                  maxLengthEnforced: true
                              )
                          ),
                          // Registered Address
                          Padding(
                            padding: Constant.PaddingDetails.textTopPadding,
                            child: TextFormField(
                              decoration: InputDecoration(
                                  border: const OutlineInputBorder(),
                                  labelText: Constant.FormLabels
                                      .REGISTERED_ADDRESS
                              ),
                              validator: (val) {
                                if (val.isEmpty)
                                  return Constant.ErrorFields.MANDATORY;
                                else return null;
                              },
                              onChanged: (val) => userDetails.registeredAddress = val,
                            ),
                          ),
                          // Physical Address
                          Padding(
                            padding: Constant.PaddingDetails.textTopPadding,
                            child: TextFormField(
                              decoration: InputDecoration(
                                  border: const OutlineInputBorder(),
                                  labelText: Constant.FormLabels
                                      .PHYSICAL_ADDRESS
                              ),
                              validator: (val) {
                                if (val.isEmpty)
                                  return Constant.ErrorFields.MANDATORY;
                                else return null;
                              },
                              onChanged: (val) => userDetails.physicalAddress = val,
                            ),
                          ),
                          // Email address
                          Padding(
                            padding: Constant.PaddingDetails.textTopPadding,
                            child: TextFormField(
                              decoration: InputDecoration(
                                  border: const OutlineInputBorder(),
                                  labelText: Constant.FormLabels.EMAIL_ADDRESS
                              ),
                              onChanged: (val) => userDetails.emailAddress = val,
                              keyboardType: TextInputType.emailAddress,
                            ),
                          ),
                          // Official website
                          Padding(
                            padding: Constant.PaddingDetails.textTopPadding,
                            child: TextFormField(
                              decoration: InputDecoration(
                                  border: const OutlineInputBorder(),
                                  labelText: Constant.FormLabels
                                      .OFFICIAL_WEBSITE
                              ),
                              validator: (val) {
                                if (val.isEmpty)
                                  return Constant.ErrorFields.MANDATORY;
                                else return null;
                              },
                              onChanged: (val) => userDetails.officialWebsite = val,
                            ),
                          ),
                          // Legal entity
                          Padding(
                              padding: Constant.PaddingDetails.textTopPadding,
                              child: new Column(
                                  children: [
                                    new Container(
                                        child: new DropdownButtonFormField(
                                          isExpanded: true,
                                          value: legalEntity,
                                          items: legalEntitiesMap.map((key, value) {
                                            return MapEntry(key,
                                                DropdownMenuItem<dynamic>(
                                                  value: value,
                                                  child: Text(value),
                                                )
                                            );
                                          }).values.toList(),
                                          validator: (value) {
                                            if (value == null)
                                              return Constant.ErrorFields
                                                  .MANDATORY;
                                            else return null;
                                          },
                                          decoration: InputDecoration(
                                              border: OutlineInputBorder(),
                                              labelText: Constant.FormLabels
                                                  .LEGAL_ENTITY
                                          ),
                                          iconEnabledColor: CupertinoColors
                                              .darkBackgroundGray,
                                          onTap: () => FocusScope.of(context)
                                              .requestFocus(new FocusNode()),
                                          onChanged: (value) {
                                            if (value == legalEntitiesMap.values
                                                .last) {
                                              setState(() {
                                                legalEntity = value;
                                                isLegalEntityOtherFieldVisible
                                                = true;
                                              });
                                            } else {
                                              setState(() {
                                                isLegalEntityOtherFieldVisible
                                                = false;
                                                legalEntity = value;
                                                if (value != legalEntitiesMap
                                                    .values.first) {
                                                  showPartnersFields = true;
                                                } else
                                                  showPartnersFields = false;
                                              });
                                            }
                                          },
                                        )// _buildLegalEntities(context, widget.apiKey),
                                    ),
                                    Visibility(
                                        visible: isLegalEntityOtherFieldVisible,
                                        child: new Padding(
                                          padding: const EdgeInsets.only(
                                              top: Constant.Layout.textViewTop
                                          ),
                                          child: TextFormField(
                                            decoration: InputDecoration(
                                                border: const OutlineInputBorder(),
                                                labelText: Constant.General.OTHER
                                            ),
                                            validator: (val) {
                                              if (val.isEmpty)
                                                return Constant.ErrorFields.MANDATORY;
                                              else return null;
                                            },
                                            onChanged: (val) => legalEntityOther = val,
                                            textCapitalization: TextCapitalization
                                                .sentences,
                                          ),
                                        )
                                    )
                                  ]
                              )
                          ),
                          // NTN/CNIC
                          Padding(
                              padding: Constant.PaddingDetails.textTopPadding,
                              child: TextFormField(
                                decoration: InputDecoration(
                                    border: const OutlineInputBorder(),
                                    labelText: Constant.FormLabels
                                        .NTN_CNIC_NUMBER
                                ),
                                validator: (val) {
                                  if (val.isEmpty)
                                    return Constant.ErrorFields.MANDATORY;
                                  else if (val.length < Constant.LengthLimit
                                      .MIN_LENGTH_3)
                                    return Constant.ErrorFields
                                        .MIN_LENGTH_ERROR_3;
                                  else if (val.length > Constant.LengthLimit
                                      .MAX_LENGTH_13)
                                    return Constant.ErrorFields
                                        .MAX_LENGTH_ERROR_13;
                                  else return null;
                                },
                                inputFormatters: [
                                  new LengthLimitingTextInputFormatter(13)
                                ],
                                keyboardType: TextInputType.number,
                                onChanged: (val) => userDetails.ntnCnicNumber = val,
                                initialValue: userDetails.ntnCnicNumber,
                              )
                          ),
                          // Name of Authorized Signatory
                          Padding(
                              padding: Constant.PaddingDetails.textTopPadding,
                              child: TextFormField(
                                decoration: InputDecoration(
                                    border: const OutlineInputBorder(),
                                    labelText: Constant.FormLabels
                                        .NAME_OF_AUTHORIZED_SIGNATORY
                                ),
                                validator: (val) {
                                  if (val.isEmpty)
                                    return Constant.ErrorFields.MANDATORY;
                                  else if (val.length < Constant.LengthLimit
                                      .MIN_LENGTH_5)
                                    return Constant.ErrorFields
                                        .MIN_LENGTH_ERROR_5;
                                  else if (val.length > Constant.LengthLimit
                                      .MAX_LENGTH_100)
                                    return Constant.ErrorFields
                                        .MAX_LENGTH_ERROR_100;
                                  else return null;
                                },
                                onChanged: (val) => userDetails
                                    .authorizedSignatoryName = val,
                                keyboardType: TextInputType.text,
                                textCapitalization: TextCapitalization
                                    .sentences,
                              )
                          ),
                          // Authorized Signatory CNIC
                          Padding(
                              padding: Constant.PaddingDetails.textTopPadding,
                              child: TextFormField(
                                  decoration: InputDecoration(
                                      border: const OutlineInputBorder(),
                                      labelText: Constant.FormLabels
                                          .AUTHORIZED_SIGNATORY_CNIC
                                  ),
                                  validator: (val) {
                                    if (val.isEmpty)
                                      return Constant.ErrorFields.MANDATORY;
                                    else if (val.length != Constant.LengthLimit
                                        .MAX_LENGTH_13)
                                      return Constant.ErrorFields
                                          .LENGTH_ERROR_13;
                                    else return null;
                                  },
                                  onChanged: (val) => userDetails
                                      .authorizedSignatoryCnic = val,
                                  keyboardType: TextInputType.number,
                                  inputFormatters: [
                                    new LengthLimitingTextInputFormatter(13)
                                  ]
                              )
                          ),
                          // Partner/Director information
                          Visibility(
                            visible: showPartnersFields,
                            child: Column(
                              children: [
                                // Partner/Director 1 info
                                Padding(
                                    padding: Constant.PaddingDetails.textTopPadding,
                                    child: TextFormField(
                                      decoration: InputDecoration(
                                          border: const OutlineInputBorder(),
                                          labelText: Constant.FormLabels.DIRECTOR_1_NAME
                                      ),
                                      validator: (val) {
                                        if (val.isEmpty)
                                          return Constant.ErrorFields.MANDATORY;
                                        else if (val.length < Constant.LengthLimit.MIN_LENGTH_5)
                                          return Constant.ErrorFields.MIN_LENGTH_ERROR_5;
                                        else if (val.length > Constant.LengthLimit.MAX_LENGTH_100)
                                          return Constant.ErrorFields.MAX_LENGTH_ERROR_100;
                                        else return null;
                                      },
                                      onChanged: (val) => partnerName1 = val,
                                      textCapitalization: TextCapitalization.sentences,
                                    )
                                ),
                                Padding(
                                    padding: Constant.PaddingDetails.textTopPadding,
                                    child: TextFormField(
                                      decoration: InputDecoration(
                                          border: const OutlineInputBorder(),
                                          labelText: Constant.FormLabels.DIRECTOR_1_CNIC
                                      ),
                                      validator: (val) {
                                        if (val.isEmpty)
                                          return Constant.ErrorFields.MANDATORY;
                                        else if (val.length > Constant.LengthLimit.MAX_LENGTH_13)
                                          return Constant.ErrorFields.MAX_LENGTH_ERROR_13;
                                        else return null;
                                      },
                                      inputFormatters: [
                                        new LengthLimitingTextInputFormatter(13)
                                      ],
                                      keyboardType: TextInputType.number,
                                      onChanged: (val) => partnerCnic1 = val,
                                    )
                                ),
                                // Partner/Director 2 info
                                Padding(
                                    padding: Constant.PaddingDetails.textTopPadding,
                                    child: TextFormField(
                                      decoration: InputDecoration(
                                          border: const OutlineInputBorder(),
                                          labelText: Constant.FormLabels.DIRECTOR_2_NAME
                                      ),
                                      validator: (val) {
                                        if (val.isEmpty)
                                          return Constant.ErrorFields.MANDATORY;
                                        else if (val.length < Constant.LengthLimit.MIN_LENGTH_5)
                                          return Constant.ErrorFields.MIN_LENGTH_ERROR_5;
                                        else if (val.length > Constant.LengthLimit.MAX_LENGTH_100)
                                          return Constant.ErrorFields.MAX_LENGTH_ERROR_100;
                                        else return null;
                                      },
                                      onChanged: (val) => partnerName2 = val,
                                      textCapitalization: TextCapitalization.sentences,
                                    )
                                ),
                                Padding(
                                    padding: Constant.PaddingDetails.textTopPadding,
                                    child: TextFormField(
                                      decoration: InputDecoration(
                                          border: const OutlineInputBorder(),
                                          labelText: Constant.FormLabels.DIRECTOR_2_CNIC
                                      ),
                                      validator: (val) {
                                        if (val.isEmpty)
                                          return Constant.ErrorFields.MANDATORY;
                                        else if (val.length > Constant.LengthLimit.MAX_LENGTH_13)
                                          return Constant.ErrorFields.MAX_LENGTH_ERROR_13;
                                        else return null;
                                      },
                                      inputFormatters: [
                                        new LengthLimitingTextInputFormatter(13)
                                      ],
                                      keyboardType: TextInputType.number,
                                      onChanged: (val) => partnerCnic2 = val,
                                    )
                                ),
                                // Partner/Director 3 info
                                Padding(
                                    padding: Constant.PaddingDetails.textTopPadding,
                                    child: TextFormField(
                                      decoration: InputDecoration(
                                          border: const OutlineInputBorder(),
                                          labelText: Constant.FormLabels.DIRECTOR_3_NAME
                                      ),
                                      validator: (val) {
                                        if (val.isEmpty)
                                          return null;
                                        else if (val.length < Constant.LengthLimit.MIN_LENGTH_5)
                                          return Constant.ErrorFields.MIN_LENGTH_ERROR_5;
                                        else if (val.length > Constant.LengthLimit.MAX_LENGTH_100)
                                          return Constant.ErrorFields.MAX_LENGTH_ERROR_100;
                                        else return null;
                                      },
                                      onChanged: (val) => partnerName3 = val,
                                      textCapitalization: TextCapitalization.sentences,
                                    )
                                ),
                                Padding(
                                    padding: Constant.PaddingDetails.textTopPadding,
                                    child: TextFormField(
                                      decoration: InputDecoration(
                                          border: const OutlineInputBorder(),
                                          labelText: Constant.FormLabels.DIRECTOR_3_CNIC
                                      ),
                                      validator: (val) {
                                        if (val.isEmpty)
                                          return null;
                                        else if (val.length > Constant.LengthLimit.MAX_LENGTH_13)
                                          return Constant.ErrorFields.MAX_LENGTH_ERROR_13;
                                        else return null;
                                      },
                                      inputFormatters: [
                                        new LengthLimitingTextInputFormatter(13)
                                      ],
                                      keyboardType: TextInputType.number,
                                      onChanged: (val) => partnerCnic3 = val,
                                    )
                                ),
                                // Partner/Director 4 info
                                Padding(
                                    padding: Constant.PaddingDetails.textTopPadding,
                                    child: TextFormField(
                                      decoration: InputDecoration(
                                          border: const OutlineInputBorder(),
                                          labelText: Constant.FormLabels.DIRECTOR_4_NAME
                                      ),
                                      validator: (val) {
                                        if (val.isEmpty)
                                          return null;
                                        else if (val.length < Constant.LengthLimit.MIN_LENGTH_5)
                                          return Constant.ErrorFields.MIN_LENGTH_ERROR_5;
                                        else if (val.length > Constant.LengthLimit.MAX_LENGTH_100)
                                          return Constant.ErrorFields.MAX_LENGTH_ERROR_100;
                                        else return null;
                                      },
                                      onChanged: (val) => partnerName4 = val,
                                      textCapitalization: TextCapitalization.sentences,
                                    )
                                ),
                                Padding(
                                    padding: Constant.PaddingDetails.textTopPadding,
                                    child: TextFormField(
                                      decoration: InputDecoration(
                                          border: const OutlineInputBorder(),
                                          labelText: Constant.FormLabels.DIRECTOR_4_CNIC
                                      ),
                                      validator: (val) {
                                        if (val.isEmpty)
                                          return null;
                                        else if (val.length > Constant.LengthLimit.MAX_LENGTH_13)
                                          return Constant.ErrorFields.MAX_LENGTH_ERROR_13;
                                        else return null;
                                      },
                                      inputFormatters: [
                                        new LengthLimitingTextInputFormatter(13)
                                      ],
                                      keyboardType: TextInputType.number,
                                      onChanged: (val) => partnerCnic4 = val,
                                    )
                                )
                              ],
                            ),
                          ),
                          // Business Category
                          Padding(
                              padding: Constant.PaddingDetails.textTopPadding,
                              child: new Column(
                                  children: [
                                    new DropdownButtonFormField(
                                      isExpanded: true,
                                      value: userDetails.businessCategory,
                                      items: businessCategoriesMap.map((key, value) {
                                        return MapEntry(key,
                                            DropdownMenuItem<dynamic>(
                                              value: value,
                                              child: Text(value),
                                            )
                                        );
                                      }).values.toList(),
                                      validator: (value) {
                                        if (value == null)
                                          return Constant.ErrorFields.MANDATORY;
                                        else return null;
                                      },
                                      decoration: InputDecoration(
                                          border: OutlineInputBorder(),
                                          labelText: Constant.FormLabels
                                              .BUSINESS_CATEGORY
                                      ),
                                      iconEnabledColor: CupertinoColors
                                          .darkBackgroundGray,
                                      onTap: () => FocusScope.of(context)
                                          .requestFocus(new FocusNode()),
                                      onChanged: (value) {
                                        if (value == businessCategoriesMap.values
                                            .last) {
                                            setState(() {
                                              isBusinessCategoryOtherFieldVisible
                                              = true;
                                              userDetails.businessCategory = value;
                                            });
                                          } else {
                                            setState(() {
                                              isBusinessCategoryOtherFieldVisible
                                              = false;
                                              userDetails.businessCategory = value;
                                            });
                                          }
                                      },
                                    ),
                                    Visibility(
                                        visible: isBusinessCategoryOtherFieldVisible,
                                        child: new Padding(
                                          padding: Constant.PaddingDetails.textTopPadding,
                                          child: TextFormField(
                                            decoration: InputDecoration(
                                                border: const OutlineInputBorder(),
                                                labelText: Constant.FormLabels.BUSINESS_CATEGORY
                                            ),
                                            validator: (val) {
                                              if (val.isEmpty)
                                                return Constant.ErrorFields.MANDATORY;
                                              else return null;
                                            },
                                            onChanged: (val) => businessCategoryOther = val,
                                            textCapitalization: TextCapitalization.sentences,
                                          ),
                                        )
                                    )
                                  ]
                              )
                          ),
                          // Office premises
                          Padding(
                            padding: Constant.PaddingDetails.textTopPadding,
                            child: new Column(
                              children: [
                                new DropdownButtonFormField(
                                  isExpanded: true,
                                  value: officeLocation,
                                  items: officePremisesMap.map((key, value) {
                                    return MapEntry(key,
                                        DropdownMenuItem<dynamic>(
                                          value: value,
                                          child: Text(value),
                                        )
                                    );
                                  }).values.toList(),
                                  onChanged: (value) {
                                    setState(() {
                                      for (var obj in officePremisesMap.entries) {
                                        if (obj.value == value) {
                                          officePremises = new OfficePremises();
                                          officePremises.id = obj.key;
                                          officePremises.name = obj.value;
                                          break;
                                        }
                                      }
                                      officeLocation = value;
                                    });
                                  },
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      labelText: Constant.FormLabels
                                          .OFFICE_PREMISES
                                  ),
                                  onTap: () => FocusScope.of(context)
                                      .requestFocus(new FocusNode()),
                                )
                              ],
                            ),
                          ),
                          // Year at current location
                          Padding(
                            padding: Constant.PaddingDetails.textTopPadding,
                            child: TextFormField(
                              decoration: InputDecoration(
                                  border: const OutlineInputBorder(),
                                  labelText: Constant.FormLabels
                                      .YEARS_CURRENT_LOCATION
                              ),
                              validator: (val) {
                                if (val.isEmpty)
                                  return Constant.ErrorFields.MANDATORY;
                                else return null;
                              },
                              onChanged: (val) => userDetails.yearAtCurrentLocation
                              = val,
                              keyboardType: TextInputType.number,
                              inputFormatters: [
                                new LengthLimitingTextInputFormatter(4)
                              ],
                            ),
                          ),
                          // Year at current business
                          Padding(
                            padding: Constant.PaddingDetails.textTopPadding,
                            child: new TextFormField(
                              decoration: InputDecoration(
                                  border: const OutlineInputBorder(),
                                  labelText: Constant.FormLabels
                                      .YEARS_CURRENT_BUSINESS
                              ),
                              validator: (val) {
                                if (val.isEmpty)
                                  return Constant.ErrorFields.MANDATORY;
                                else return null;
                              },
                              onChanged: (val) => userDetails.yearAtCurrentBusiness
                              = val,
                              keyboardType: TextInputType.number,
                              inputFormatters: [
                                new LengthLimitingTextInputFormatter(4)
                              ],
                            ),
                          ),
                          // Expected Monthly TurnOver
                          Padding(
                              padding: Constant.PaddingDetails.textTopPadding,
                              child: new TextFormField(
                                decoration: InputDecoration(
                                    border: const OutlineInputBorder(),
                                    labelText: Constant.FormLabels
                                        .EXPECTED_MONTHLY_TURNOVER,
                                    prefix: Text(Constant.General.PAK_RUPEE)
                                ),
                                validator: (val) {
                                  if (val.isEmpty)
                                    return Constant.ErrorFields.MANDATORY;
                                  else return null;
                                },
                                onChanged: (val) => userDetails.expectedMonthlyTurnOver
                                = val,
                              )
                          ),
                          // Bank Name
                          Padding(
                            padding: Constant.PaddingDetails.textTopPadding,
                            child: new TextFormField(
                              decoration: InputDecoration(
                                  border: const OutlineInputBorder(),
                                  labelText: Constant.FormLabels.BANK_NAME
                              ),
                              validator: (val) {
                                if (val.isEmpty) {
                                  return Constant.ErrorFields.MANDATORY;
                                } else return null;
                              },
                              onChanged: (val) => businessDetails.bankName
                              = val,
                              textCapitalization: TextCapitalization.sentences,
                            ),
                          ),
                          // Branch Name
                          Padding(
                            padding: Constant.PaddingDetails.textTopPadding,
                            child: new TextFormField(
                              decoration: InputDecoration(
                                  border: const OutlineInputBorder(),
                                  labelText: Constant.FormLabels.BRANCH_NAME
                              ),
                              validator: (val) {
                                if (val.isEmpty) {
                                  return Constant.ErrorFields.MANDATORY;
                                } else return null;
                              },
                              onChanged: (val) => businessDetails.branchName
                              = val,
                              textCapitalization: TextCapitalization.sentences,
                            ),
                          ),
                          // Account title
                          Padding(
                            padding: Constant.PaddingDetails.textTopPadding,
                            child: new TextFormField(
                              decoration: InputDecoration(
                                  border: const OutlineInputBorder(),
                                  labelText: Constant.FormLabels.ACCOUNT_TITLE
                              ),
                              validator: (val) {
                                if (val.isEmpty) {
                                  return Constant.ErrorFields.MANDATORY;
                                } else if (val.length < Constant.LengthLimit
                                    .MIN_LENGTH_5) {
                                  return Constant.ErrorFields
                                      .MIN_LENGTH_ERROR_5;
                                } else if (val.length > Constant.LengthLimit
                                    .MAX_LENGTH_100) {
                                  return Constant.ErrorFields
                                      .MAX_LENGTH_ERROR_100;
                                } else return null;
                              },
                              onChanged: (val) => businessDetails.accountTitle
                              = val,
                              textCapitalization: TextCapitalization.sentences,
                            ),
                          ),
                          // Branch Code
                          Padding(
                            padding: Constant.PaddingDetails.textTopPadding,
                            child: new TextFormField(
                                decoration: InputDecoration(
                                    border: const OutlineInputBorder(),
                                    labelText: Constant.FormLabels.BRANCH_CODE
                                ),
                                validator: (val) {
                                  if (val.isEmpty) {
                                    return Constant.ErrorFields.MANDATORY;
                                  } else return null;
                                },
                                onChanged: (val) => businessDetails.branchCode
                                = val
                            ),
                          ),
                          // Account Number
                          Padding(
                            padding: Constant.PaddingDetails.textTopPadding,
                            child: new TextFormField(
                              decoration: InputDecoration(
                                  border: const OutlineInputBorder(),
                                  labelText: Constant.FormLabels.ACCOUNT_NUMBER
                              ),
                              validator: (val) {
                                if (val.isEmpty) {
                                  return Constant.ErrorFields.MANDATORY;
                                } else if (val.length < Constant.LengthLimit
                                    .MIN_LENGTH_1) {
                                  return Constant.ErrorFields
                                      .MIN_LENGTH_ERROR_1;
                                } else return null;
                              },
                              inputFormatters: [
                                new LengthLimitingTextInputFormatter(10)
                              ],
                              keyboardType: TextInputType.number,
                              onChanged: (val) => businessDetails.accountNumber
                              = val,
                            ),
                          ),
                          // IBan
                          Padding(
                            padding: Constant.PaddingDetails.textTopPadding,
                            child: new TextFormField(
                                decoration: InputDecoration(
                                    border: const OutlineInputBorder(),
                                    labelText: Constant.FormLabels.IBAN
                                ),
                                validator: (val) {
                                  if (val.isEmpty) {
                                    return Constant.ErrorFields.MANDATORY;
                                  } else return null;
                                },
                                onChanged: (val) => businessDetails.iBan = val
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                new Align(
                    alignment: Alignment.bottomCenter,
                    child: new Container(
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          // Foree Logo
                          new Container(
                              width: Constant.Layout.dp100,
                              height: Constant.Layout.dp50,
                              child: new Image.network(Constant.AssetsPath.FOREE_LOGO)
                          ),
                          new Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              // Button save
                              Padding(
                                padding: const EdgeInsets.only(left: Constant
                                    .Layout.dp5),
                                child: new ElevatedButton(
                                  onPressed: () async {
                                    if ((userDetails.mobileNumber == null ||
                                        userDetails.mobileNumber.isEmpty) &&
                                        (userDetails.emailAddress == null ||
                                            userDetails.emailAddress.isEmpty)) {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                          new SnackBar(
                                              content: new Text(
                                                  "Mobile number or Email address cannot be empty at same time."
                                              )
                                          )
                                      );
                                    } else {
                                      if (file != null && !isLogoUploaded) {
                                        UploadImageResponse response;
                                        Utilities.prepareFormData(this.file, Constant.JsonFieldKey.BUSINESS_LOGO,
                                            "7ce88ad8-7420-4068-accb-ed58685e607e").then((formData) =>
                                        {
                                          Utilities.getClient(widget.apiKey).uploadImageToServer(formData)
                                              .then((value) => {
                                            response = UploadImageResponse.fromJson(json.decode(value)),
                                            if (response.responseCode == 0) {
                                              isLogoUploaded = true
                                            } else {
                                              logoPath = "",
                                              showLogoUploadIcon = false,
                                              isLogoUploaded = false
                                            },
                                            ScaffoldMessenger.of(context).showSnackBar(new SnackBar(
                                              content: new Text(response.responseData),
                                            ))
                                          })
                                        });
                                      }
                                      userDetails.formSubmitted = false;
                                      userDetails.businessDetails = businessDetails;
                                      userDetails.officePremises = officePremises;
                                      if (legalEntity != null && legalEntity.isNotEmpty) {
                                        for (var obj in legalEntitiesMap.entries) {
                                          if (obj.value == legalEntity) {
                                            legalEntities.id = obj.key;
                                            legalEntities.name = obj.value;
                                            break;
                                          }
                                        }
                                      }
                                      userDetails.legalEntity = legalEntities;
                                      SignUpResponse res;
                                      await Utilities.signUpCall(widget.apiKey,
                                          userDetails).then((value) => {
                                        res = SignUpResponse.fromJson(json.decode(value)),
                                        if (res.businessId != null) {
                                          userDetails.businessId =
                                              res.businessId,
                                        } else {
                                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                            content: Text(res.errorMessage),
                                          ))
                                        }
                                      });
                                    }
                                  },
                                  child: new Text(Constant.ButtonTags.SAVE),
                                ),
                              ),
                              // Button Next
                              Padding(
                                padding: const EdgeInsets.only(left: Constant
                                    .Layout.dp5),
                                child: new ElevatedButton(
                                    onPressed: () async {
                                      if (_formKey.currentState.validate()) {
                                      Navigator.push(_formKey.currentContext,
                                          new MaterialPageRoute(
                                              builder: (context) => new
                                              BusinessDocuments(
                                                title: Constant.PageTitle
                                                    .BUSINESS_DOCUMENTS,
                                                data: rawResult, logo: logo,
                                                apiKey: widget.apiKey,
                                              )
                                          )
                                      );
                                      }
                                    },
                                    child: new Text(Constant.ButtonTags.NEXT)
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    )
                )
              ],
            ),
          ),
        )
    ); // _buildBody(context, widget.apiKey);
  }

  Future<void> uploadImage(FileType type) async {
    FilePicker.getFile(type: type).then((file) => {
      setState(() {
        UploadImageResponse response;
        if (file != null) {
          if ((userDetails.mobileNumber == null || userDetails.mobileNumber.isEmpty) &&
              (userDetails.emailAddress == null || userDetails.emailAddress.isEmpty)) {
            logoPath = file.path;
            showLogoUploadIcon = false;
            isLogoUploaded = false;
            this.file = file;
          } else {
            Utilities.prepareFormData(file, Constant.JsonFieldKey.BUSINESS_LOGO,
                "7ce88ad8-7420-4068-accb-ed58685e607e").then((formData) =>
            {
              Utilities.getClient(widget.apiKey).uploadImageToServer(formData)
                  .then((value) => {
                response = UploadImageResponse.fromJson(json.decode(value)),
                if (response.responseCode == 0) {
                  logoPath = file.path,
                  showLogoUploadIcon = false,
                  isLogoUploaded = true
                } else {
                  logoPath = "",
                  showLogoUploadIcon = false,
                  isLogoUploaded = false
                },
                ScaffoldMessenger.of(context).showSnackBar(new SnackBar(
                  content: new Text(response.responseData),
                ))
              })
            });
          }
        } else {
          showLogoUploadIcon = false;
          isLogoUploaded = false;
        }
      })
    });
  }
  void _removeLogo() {
    setState(() {
      showLogoUploadIcon = true;
      logoPath = "";
    });
  }
}