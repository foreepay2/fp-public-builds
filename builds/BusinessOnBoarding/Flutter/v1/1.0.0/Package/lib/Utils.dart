import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';

import 'Responses/AppVersionInfoResponse.dart';
import 'RetrofitConfigForee.dart';
import 'Constants.dart';

class Utilities {
  static ForeeRestClient client;
  static String getImageBase64(String image) {
    final bytes = File(image).readAsBytesSync();
    return base64Encode(bytes);
  }
  static Future<String> uploadImage(FileType type) async {
    return await FilePicker.getFilePath(type: type);
  }
  static ForeeRestClient getClient(String apiKey) {
    if (client == null) {
      final dio = Dio(BaseOptions(contentType: "application/json"));
      dio.options.headers[RequestKey.X_AGGREGATOR_KEY] = apiKey;
      client = new ForeeRestClient(dio);
    }
    return client;
  }
  static Future<AppVersionInfoResponse> versionInfoCall(String apiKey,
      String appVersion) async {
    AppVersionInfoResponse info;
    if (apiKey != null && apiKey.isNotEmpty) {
      VersionInfo version = new VersionInfo();
      version.sdkPlatform = "flutter";
      version.sdkAppVersion = appVersion;
      final response = await getClient(apiKey).getVersion(version);
      info = AppVersionInfoResponse.fromJson(
          jsonDecode(response)[ResponseKey.APP_VERSION_INFO]
      );
    }
    return info;
  }
  static Future<String> signUpCall(String apiKey, UserDetails userDetails) async {
    String response;
    if (apiKey != null && apiKey.isNotEmpty)
      response = await getClient(apiKey).signUp(userDetails);
    return response;
  }
  static Future<String> fetchLegalEntities(String apiKey) async {
    String legalEntities;
    if (apiKey != null && apiKey.isNotEmpty)
      legalEntities = await getClient(apiKey).fetchLegalEntities();
    return legalEntities;
  }
  static Future<String> fetchBusinessCategories(String apiKey) async {
    String businessCategories;
    if (apiKey != null && apiKey.isNotEmpty) {
      businessCategories = await getClient(apiKey).fetchBusinessCategories();
    }
    return businessCategories;
  }
  static void sendImageToServer(File file, String apiKey, String fieldKey) async {
    var url = 'https://api-dev5.foreebill.com/aggregator/business/upload-docs';
    FormData data = FormData.fromMap({
      JsonFieldKey.BUSINESS_ID: "7ce88ad8-7420-4068-accb-ed58685e607e",
      fieldKey: await MultipartFile.fromFile(
        file.path,
        filename: file.path.split("/").last,
      ),
    });
    Dio dio = new Dio();
    dio.options.headers[RequestKey.X_AGGREGATOR_KEY] = apiKey;
    dio.post(url, data: data).then((response) {
      var jsonResponse = jsonDecode(response.toString());
      print(jsonResponse);
    }).catchError((error) => print(error));
  }
  static Future<FormData> prepareFormData(File file, String fieldKey,
      String businessId) async {
    return FormData.fromMap({
      JsonFieldKey.BUSINESS_ID: businessId,
      fieldKey: await MultipartFile.fromFile(
        file.path,
        filename: file.path.split("/").last,
      ),
    });
  }
  static Future<String> fetchOfficePremises(String apiKey) async {
    String officePremises;
    if (apiKey != null && apiKey.isNotEmpty)
      officePremises = await getClient(apiKey).fetchOfficePremises();
    return officePremises;
  }
}